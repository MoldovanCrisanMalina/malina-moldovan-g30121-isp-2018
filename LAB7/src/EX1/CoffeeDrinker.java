package EX1;

public class CoffeeDrinker {

    public void drinkCofee(Coffe c) throws TemperatureException, ConcentrationException {
        if (c.getTemp() > 60)
            throw new TemperatureException(c.getTemp(), "Cofee is too hot!");
        if (c.getConc() > 50)
            throw new ConcentrationException(c.getConc(), "Cofee concentration too high!");
        System.out.println("Drink cofee:" + c);
    }
}
