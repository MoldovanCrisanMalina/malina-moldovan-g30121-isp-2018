package EX1;

public class Coffee {
    private int temp;
    private int conc;

    Coffee(int t, int c) {
        this.temp = t;
        this.conc = c;
    }

    int getTemp() {
        return temp;
    }

    int getConc() {
        return conc;
    }

    public String toString() {
        return "[cofee temperature=" + this.temp + ":concentration=" + this.conc + "]";
    }

}

