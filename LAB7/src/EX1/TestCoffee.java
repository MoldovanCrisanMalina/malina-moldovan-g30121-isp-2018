package EX1;

public class TestCoffee {

    public static void main(String[] args) {
        CoffeeMaker mk = new CoffeeMaker();
        CoffeeDrinker d = new CoffeeDrinker();

        for (int i = 0; i < 5; i++) {
            Coffe c = mk.makeCoffee();

            try {
                if (i == 2) {

                    throw new IllegalArgumentException();
                }
                d.drinkCofee(c);

            } catch (IllegalArgumentException e) {
                System.out.println(" " + "too more coffee");
                break;
            } catch (TemperatureException e) {
                System.out.println("Exception:" + e.getMessage() + " temp=" + e.getTemp());
            } catch (ConcentrationException e) {
                System.out.println("Exception:" + e.getMessage() + " conc=" + e.getConc());
            } finally {
                System.out.println("Throw the coffee cup.\n");
            }
        }
    }
}
