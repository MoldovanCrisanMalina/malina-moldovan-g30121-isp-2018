package EX2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Counter implements  ActionListener{

    JButton increment = new JButton("increment");
    JFrame frm = new JFrame();
    JTextField tField = new JTextField(15);

    int i = 0;

    public Counter() {

        tField.setText("" + i);
        frm.setTitle("Counter");
        frm.setVisible(true);
        frm.setSize(200, 100);

        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frm.setResizable(true);
        frm.setLayout(new FlowLayout());
        frm.add(tField);
        frm.add(increment);
        increment.addActionListener(this);


    }
    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == increment) {
            i++;
            tField.setText("" + i);

        }
    }

    public static void main(String[] args) {
        new Counter();
    }





}
