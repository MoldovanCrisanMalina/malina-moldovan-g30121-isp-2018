package MALINA_EX1;

abstract class Shape {
    protected String color;
    protected boolean filled;

    public Shape() {
    }

    public Shape(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
    }

    public String getColor() {
        return color;

    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    abstract double getAria();
    abstract  double getPerimetru();


    public void tooString() {
        if (this.filled == true) {
            System.out.println("A shape with color of " + this.color + "and filled");
        }
        if (this.filled == false) {
            System.out.println("A shape with color of " + this.color + "and not filled");

        }
    }
}
