package MALINA_EX1;


public class Square extends Rectangle{
    public Square() {
    }

    public Square(double side) {
        this.width = Math.sqrt(side);
        this.width = this.length;
    }

    public Square(double side, String color, boolean filled) {
        this.width = Math.sqrt(side);
        this.width = this.length;
        this.color = color;
        this.filled = filled;


    }

    public double getSide() {
        return this.width * this.length;
    }

    public void setSide(double side) {
        this.width = Math.sqrt(side);
        this.width = this.length;
    }

    public void getWidth(double side) {
        this.width = Math.sqrt(side);
        this.width = this.length;
    }

    public void getLength(double side) {
        this.width = Math.sqrt(side);
        this.width = this.length;
    }
    public double getAria(){
        return  this.getSide()*this.getSide();
    }
    public double getPerimetru(){
        return 4*this.getSide();
    }
    public String toString() {
        return "A Square with side=" + this.getSide() + " ,which is a subclass of" + this.toString();
    }
}
