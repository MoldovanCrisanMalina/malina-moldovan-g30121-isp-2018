package MALINA_EX4;

import MALINA_EX3.LightSensor;
import MALINA_EX3.TemperatureSensor;


public class Singleton {

    private Singleton() {

    }

    ;
    private static Singleton Controller;


    public static void control() throws InterruptedException {


        TemperatureSensor s1 = new TemperatureSensor();
        LightSensor s2 = new LightSensor();


        int sec = 1;
        while (sec <= 10) {
            System.out.println("Temp: " + s1.readValue());
            System.out.println("Light: " + s2.readValue());
            System.out.println("Sec: " + sec);
            sec++;
            Thread.sleep(1000);
        }


    }


    public static void main(String[] args) throws InterruptedException {

        Controller.control();

    }
}