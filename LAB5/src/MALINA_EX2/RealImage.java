package MALINA_EX2;

public class RealImage implements Image {

    private String fileName;

    public RealImage(String fileName) {
        this.fileName = fileName;
        loadFromDisk(fileName);
    }
    public void RotatedImage()
    {
        System.out.println("Rotated Image "+fileName);
    }
    @Override
    public void display(){
        System.out.println("Display real image "  +fileName);
    }
    public void loadFromDisk(String fileName){
        System.out.println("Loading  real image "+fileName);
    }
}
