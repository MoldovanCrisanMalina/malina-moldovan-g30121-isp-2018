package MALINA_EX2;

public class TestImage {
    public static void main(String[] args) {
        RealImage RealImg = new RealImage("Img.jpg");
        ProxyImage ProxyImg = new ProxyImage("Img1.jpg", "left");
        ProxyImage ProxyImg2 = new ProxyImage("Img2.jpg");

        RealImg.display();
        ProxyImg.display();
        ProxyImg2.display();
    }
}
