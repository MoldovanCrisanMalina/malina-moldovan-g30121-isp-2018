package MALINA_EX3;

abstract class Sensor {

    private String location;

    abstract public int readValue();
    public String getLocation(){
        return this.location;

    }

}
