package MALINA_EX3;

public class Controller{

    public void control() throws InterruptedException{

        Sensor s1=new TemperatureSensor();
        Sensor s2= new LightSensor();

        int sec=1;
        while (sec<=20){
            System.out.println("temp:" +s1.readValue());
            System.out.println("light:" +s2.readValue());
            System.out.println("sec: " +sec);
            sec++;
            Thread.sleep(1000);
        }
    }
}

