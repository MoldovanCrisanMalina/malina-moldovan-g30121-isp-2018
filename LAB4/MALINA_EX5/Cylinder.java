package MALINA_EX5;
import MALINA_EX1.Circle ;

public class Cylinder extends Circle {

    double height = 1.0;

    public Cylinder() {}


    public Cylinder(double radius) {
        super(radius);
    }

    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public double getVolume() {
        return height * Math.PI * super.getRadius() * super.getRadius();
    }

    public double getAria() {
        return 2 * 3.14 * super.getRadius() * this.height + 2 * super.getAria();
    }

}
