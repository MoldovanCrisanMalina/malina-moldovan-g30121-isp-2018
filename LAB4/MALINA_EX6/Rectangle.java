package MALINA_EX6;

public class Rectangle {
    public class Rectangle extends Shape {

        double width = 1.0;
        double length = 1.0;

        public Rectangle() {
        }

        public Rectangle(double width, double length) {
            this.width = width;
            this.length = length;
        }

        public Rectangle(double width, double length, String color, boolean filled) {
            super(color, filled);
            this.width = width;
            this.length = length;
        }
        public double getWidth(){
            return width;
        }
        public void setWidth(double width){
            this.width=width;
        }
        public double getLength(){
            return length;
        }
        public void setLength(double length){
            this.length=length;
        }
        public double getAria(){
            return this.length*this.width;
        }
        public double getPerimetru(){
            return 2*(this.length+this.width);
        }
        public void tooString(){
            System.out.println("A rectangle with width:" +this.width + "and length" +this.length +"which is a subclass of");
            super.tooString();
        }
    }

}
