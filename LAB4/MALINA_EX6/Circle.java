package MALINA_EX6;

public class Circle {
    package magdalena_sangeorzan_lab4_ex6;

    public class Circle extends Shape {
        double radius = 1.0;

        public Circle() {
        }

        public Circle(double radius) {
            this.radius = radius;
        }

        public Circle(double radius, String color, boolean filled) {
            super(color, filled);
            this.radius = radius;
        }

        public double getRadius() {
            return radius;
        }

        public void setRadius(double radius) {
            this.radius = radius;
        }

        public double getAria() {
            return 3.14 * radius * radius;
        }

        public double getPerimetru() {
            return 2 * 3.14 * radius;
        }

        public void tooString() {
            System.out.println("A circle with radius:" + this.radius + "which is a subclass of:");
            super.tooString();

        }


    }

}
