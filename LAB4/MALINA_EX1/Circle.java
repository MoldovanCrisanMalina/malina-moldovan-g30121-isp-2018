package MALINA_EX1;

public class Circle {


    private double radius = 1.0;
    private String color = "red";


    public Circle() {
    }

    public Circle(double r) {

        this.radius = r;


    }

    public double getRadius() {
        return radius;

    }

    public double getAria() {
        return 3.14 * radius * radius;
    }

    public String toString(){
        return "color is:" +color;

    }

}

