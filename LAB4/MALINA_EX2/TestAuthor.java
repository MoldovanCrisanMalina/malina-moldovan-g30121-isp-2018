package MALINA_EX2;

public class TestAuthor {

    public static void main(String[] args){

        Author a1=new Author("Malina","malinamoldovan",'f');

        String a = a1.getName();
        System.out.println("getName: " + a);

        String b = a1.getEmail();
        System.out.println("getEmail: " + b);

        char c = a1.getGender();
        System.out.println("getGender: " + c);

        a1.setEmail("dragos_shakeit@yahoo.com");

        String d = a1.getEmail();
        System.out.println("getEmail after using setEmail: " + d + "\n");


    }

}
