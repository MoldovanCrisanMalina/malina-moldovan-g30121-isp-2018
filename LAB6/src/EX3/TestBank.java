package EX3;

public class TestBank{
    public static void main(String[] args) {
        Bank bank = new Bank();
        bank.addAccount("Daia", 8000);
        bank.addAccount("Ioana", 9050);
        bank.addAccount("Andreea", 7000);
        bank.addAccount("Carina", 7800);

        System.out.println("Accounts by balance");
        bank.printAccounts();

        System.out.println("Accounts between limits");
        bank.printAccounts(7500, 9000);

        System.out.println("Account by owner name and balance");
        bank.displayAccount("Daiana", 8000);

        System.out.println("Get All Account ");
        bank.displayAllAccounts();

    }
}

