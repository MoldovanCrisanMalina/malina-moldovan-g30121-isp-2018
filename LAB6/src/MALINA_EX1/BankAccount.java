package MALINA_EX1;

public class BankAccount {
    private String owner;
    private double balance;

    BankAccount(String  owner, balance){
        this.owner = owner;
        this.balance = balance;
    }
    public void widthraw(double sum){
        if(balance<sum)
        {
            System.out.println("You don't have enough money");
        }
        if((balance-sum)>0)
        {
            System.out.println("You can make this trasaction");
        }
    }
    public int hashCode(){
        return owner.hashCode()+(int)balance;
    }


    @Override
    public boolean equals(Object obj) {
        if(obj instanceof BankAccount){
            BankAccount p = (BankAccount) obj;
            return balance == p.balance;
        }
        return false;
    }
}
